## Known Bugs

This is a long series teaching how to buid a blog system. Everything is caught on video which greatly slows down the development process. Also with so many skills needing to be taught in such a short period of time, many bugs will naturally arise as they do in any development project. What makes things tricky is that after a video goes live, it is difficult to highlight and impliment bug fixes. Source code needs to stay in sync with the videos, many of which do not highlight these bugs fixes.

The solution is to create a bug log of known bugs and known resolutions that can be fixed at a later time.

- **Part 24:** On line 3 of our `resources/views/blog/single.blade.php` file we call `$post->title` without escaping the data (or sanitizing it prior to saving in the DB). **This makes the application vulnerable to XSS.**

	- Solution: the `$post->title` should be wrapped in `htmlspecialchars()` like this: `htmlspecialchars($post->title)`. [View the issue on GitHub](https://github.com/jacurtis/laravel-blog-tutorial/issues/1).


- **Part 38:** When a user leaves the tag multi-select box empty upon creating a new tag, an error occurs because we do not check if the tag field is empty like we do when updating a tag.

	- Solution: Add an `if` statement to check if the tag field has anything in it, if it does, then sync, otherwise do nothing.
